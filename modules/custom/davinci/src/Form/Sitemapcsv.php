<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Add custom URLs to XML Sitemap 
 *
 * This is a companion for the XML Sitemap module. 
 * It makes it easier to add multiple URLs to the sitemap easily. 
 * Please see https://github.com/qlstudent/csv 
 * to use this outside of drupal
 *
 * PHP version 7
 *
 * @category   Search
 * @package    DaVinci
 * @author     Original Author <author@example.com>
 * @author     Another Author <another@example.com>
 * @copyright  2018 Queens Library
 * @license    http://www.queenslibrary.org  Proprietary?
 * @version    GIT: $Id$
 * @link       https://www.drupal.org/project/xmlsitemap
 * @see        XMLSitemap
 * @since      File available since May 2018
 * @deprecated Not deprecated 
 */
namespace Drupal\davinci\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;


/**
 * This module will add/replace custom sitemaps in the xmlsitemap table
 *
 * @category   Search
 * @package    DaVinci
 * @author     Original Author <author@example.com>
 * @author     Another Author <another@example.com>
 * @copyright  2018 Queens Library
 * @license    http://www.queenslibrary.org  Proprietary?
 * @version    Release: @0.0.0@
 * @link       https://www.drupal.org/project/xmlsitemap
 * @see        DaVinci, Net_Sample::Net_Sample()
 * @since      File available since May 2018
 * @deprecated Not deprecated 
 */
class Sitemapcsv extends FormBase
{
    /** 
     * Get the form id
     * 
     * @return String returns 'csvform'
     */
    public function getFormId() 
    {
        return 'csvform';
    }

    /**
     * Build form 
     * 
     * @param array              $form       Drupal form
     * @param FormStateInterface $form_state Form state
     * 
     * @return array             $form
     */
    public function buildForm(array $form, FormStateInterface $form_state) 
    {
        $markup1 = "Batch upload custom addresses in bulk. ";
        $explainRequired = "Fields marked with a red asterisk are required fields. ";

        $form['step1']['markup1'] = array(
            '#type' => 'markup',
            '#markup' => $markup1,
        );

        $form['explainRequired']['markup1'] = array(
            '#type' => 'markup',
            '#markup' => $explainRequired,
        );
        
        // $form['filepath'] = array(
        //   '#type' => 'textfield',
        //   '#title' => t('Please enter the site address for a sitemap CSV file'),
        //   '#required' => true,
        //   '#placeholder' => 'public://2018-05/sitemap.csv ',
        // );

        $form['deleteExisting'] = array(
            '#type' => 'checkbox',
            '#title' => t(
                'Delete existing custom addresses before uploading?'
            ),
            '#placeholder' => 'public://2018-05/sitemap.csv ',
        );

        $form['myFile'] = array(
            '#type' => 'managed_file',
            '#title' => t('Please enter the site address for a sitemap CSV file'),
            '#upload_validators' => array(
                'file_validate_extensions' => array(),
              ),
            '#required' => 'true',
          );

        $form['submit'] = array(
            '#type' => 'submit',
            '#value' => $this->t('Submit')
        );

        return $form;
    }

    /**
     * Validate form 
     * 
     * We don't do anything here 
     * 
     * @param array              $form       Drupal form
     * @param FormStateInterface $form_state Form state
     * 
     * @return array             $form
     */
    public function validateForm(array &$form, FormStateInterface $form_state) 
    {
        
    }

    /**
     * Submit form 
     * 
     * Get the file location and load data from the file 
     * We might want to do some validation later maybe v2 
     * 
     * @param array              $form       Drupal form
     * @param FormStateInterface $form_state Form state
     * 
     * @return array             $form
     */    
    public function submitForm(array &$form, FormStateInterface $form_state) 
    {
        /* Fetch the array of the file stored temporarily in database */
        $myCSV = $form_state->getValue('myFile');
        if ($myCSV != null) { 
            $fileCSV = \Drupal\file\Entity\File::load($myCSV[0]);
            $pathCSV = $fileCSV->get('uri')->value;
            $plainTextPath = trim($pathCSV);
            \Drupal::logger('davinci')
                ->notice("About to add custom sitemap addresses from {$plainTextPath}.");
            $localPath = \Drupal::service('file_system')->realpath($plainTextPath);
            if (($handle = fopen($localPath, "r")) !== false) {
                $setBufferSize 
                    = "SET session bulk_insert_buffer_size= 2000 * 1024 * 1024";
                $deleteAllCustomAddresses 
                    = "DELETE FROM {xmlsitemap} WHERE type = 'custom'";
                $loadData = "LOAD DATA LOCAL INFILE '" . $localPath 
                    . "' INTO TABLE {xmlsitemap} FIELDS TERMINATED BY ',' ignore 1 lines";

                // set up an extra db connection
                $connection = \Drupal::database()->getConnectionOptions();
                $connection['pdo'][\PDO::MYSQL_ATTR_LOCAL_INFILE] = 1;
                // var_dump($connection['pdo'][\PDO::ATTR_ERRMODE]); 
                // var_dump(\PDO::ERRMODE_EXCEPTION);
                // die();
                \Drupal\Core\Database\Database::addConnectionInfo(
                    'extra', 'default', $connection
                );
                \Drupal\Core\Database\Database::setActiveConnection('extra');

                // instantiate the extra db connection and query against it
                $db = \Drupal\Core\Database\Database::getConnection();
                $db->query($setBufferSize)->execute();
                if ($form_state->getValue('deleteExisting') == 1) { 
                    $db->query($deleteAllCustomAddresses)->execute();
                }
                try {
                    $db->query($loadData)->execute();
                } catch (Exception $e) {
                    var_dump($e);
                    $errorMessage = "Sorry. We failed to load the data. ";
                    if ($e->errorInfo[1] == 1062) {
                        $errorMessage .= "Please check for duplicates.";
                    }
                    Drupal::logger('davinci')->error($e);
                    drupal_set_message(t($errorMessage), 'error');
                    die();
                }

                $successMessage = "Adding from {$localPath} is complete. ";
                $successMessage .= "Please check " 
                . "/admin/config/search/xmlsitemap/custom "
                . "to verify that the loading was successful. "
                . "Loading fails if we have duplicate primary keys.";

                // set connection back to default;
                \Drupal\Core\Database\Database::setActiveConnection();
                \Drupal::logger('davinci')
                ->notice($successMessage);
                drupal_set_message($successMessage, 'warning');
            } else {
                $explainFileDoesNotExist 
                    = "I could not find the file at {$plainTextPath}";
                \Drupal::logger('davinci')->error($explainFileDoesNotExist);
                drupal_set_message(t($explainFileDoesNotExist), 'error');
            }
        } else { 
            $explainFileDoesNotExist = "I could not find the file at null";
                \Drupal::logger('davinci')->error($explainFileDoesNotExist);
                drupal_set_message(t($explainFileDoesNotExist), 'error');
        }
    }
}
